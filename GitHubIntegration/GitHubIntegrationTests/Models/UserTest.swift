//
//  UserTest.swift
//  GitHubIntegration
//
//  Created by Thiago Mattara on 05/12/16.
//  Copyright © 2016 Thiago Mattara. All rights reserved.
//

import XCTest
import Nimble
import ObjectMapper

@testable import GitHubIntegration

class UserTest: XCTestCase {
    
    func testUser(){
        //Arrange & Act
        let user = User()
        
        //Assert
        expect(user.login).to(beNil())
        expect(user.pictureUrl).to(beNil())
    }
    
    func testUserWithFullJSON(){
        //Arrange
        let json = ["login":"LoginFake", "avatar_url" : "no_picture"]
        
        //Act
        let user = User(JSON: json)
        
        //Assert
        expect(user!.login).to(equal(json["login"]))
        expect(user!.pictureUrl).to(equal(json["avatar_url"]))
    }
    
    func testUserWithJSONWithOtherInformations(){
        //Arrange
        let json = ["id":"1", "pass" : "***"]
        
        //Act
        let user = User(JSON: json)
        
        //Assert
        expect(user!.login).to(beNil())
        expect(user!.pictureUrl).to(beNil())
    }
}
