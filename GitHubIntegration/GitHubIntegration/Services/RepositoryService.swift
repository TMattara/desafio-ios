//
//  RepositoryService.swift
//  GitHubIntegration
//
//  Created by Thiago Mattara on 03/12/16.
//  Copyright © 2016 Thiago Mattara. All rights reserved.
//

import Foundation
import Alamofire
import When
import ObjectMapper

class RepositoryService {
    
    static func SearchRepositories(query: String, sortBy: String, page: Int) -> Promise<[Repository]>{
        let promise = Promise<[Repository]>()
        
        Alamofire
            .request(
                "https://api.github.com/search/repositories?q=\(query)&sort=\(sortBy)&page=\(page)",
                method: HTTPMethod.get
            )
            .responseJSON { response in
                var repositories = [Repository]()
                
                if let itens = response.result.value! as? [String:AnyObject] {
                    repositories = Mapper<Repository>().mapArray(JSONObject: itens["items"])!
                }
                
                promise.resolve(repositories)
        }
        
        return promise
    }
    
    static func GetPullRequestsFromRepository(repository:Repository) -> Promise<[PullRequest]>{
        let promise = Promise<[PullRequest]>()
        
        Alamofire
            .request(
                "https://api.github.com/repos/\(repository.fullName!)/pulls",
                method: HTTPMethod.get
            )
            .responseString { response in
                var pullRequests = [PullRequest]()
                
                if let stringResponse = response.result.value {
                    pullRequests = Mapper<PullRequest>().mapArray(JSONString: stringResponse)!
                }
                
                promise.resolve(pullRequests)
        }
        
        return promise
    }
}
