//
//  PullRequestCell.swift
//  GitHubIntegration
//
//  Created by Thiago Mattara on 04/12/16.
//  Copyright © 2016 Thiago Mattara. All rights reserved.
//

import Foundation
import UIKit
import MapleBacon

class PullRequestCell: UITableViewCell {
    @IBOutlet weak var pullRequestTitleLabel : UILabel!
    @IBOutlet weak var pullRequestBodyLabel : UILabel!
    @IBOutlet weak var pullRequestLoginLabel : UILabel!
    @IBOutlet weak var pullRequestDateLabel : UILabel!
    @IBOutlet weak var ownerPicture : UIImageView!
    
    static func returnCellFromPullRequest(pullRequest: PullRequest) -> PullRequestCell{
        let cell = Bundle.main.loadNibNamed("PullRequestCell", owner: self, options: nil)?.first as! PullRequestCell
        
        cell.pullRequestBodyLabel.text = pullRequest.body != nil ? pullRequest.body! : ""
        cell.pullRequestLoginLabel.text = pullRequest.owner != nil && pullRequest.owner!.login != nil ? pullRequest.owner!.login! : ""
        cell.pullRequestTitleLabel.text = pullRequest.title != nil ? pullRequest.title! : ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        cell.pullRequestDateLabel.text =  dateFormatter.string(from: pullRequest.date!)
        
        cell.ownerPicture.setImage(withUrl: NSURL(string: pullRequest.owner!.pictureUrl!) as! URL)
        
        return cell
    }
}
