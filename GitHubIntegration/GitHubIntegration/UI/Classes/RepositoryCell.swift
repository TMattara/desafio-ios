//
//  RepositoryCell.swift
//  GitHubIntegration
//
//  Created by Thiago Mattara on 03/12/16.
//  Copyright © 2016 Thiago Mattara. All rights reserved.
//

import Foundation
import UIKit
import MapleBacon

class RepositoryCell : UITableViewCell{
    @IBOutlet weak var repositoryNameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var forksLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var ownerPicture : UIImageView!
    
    static func returnCellFromRepository(repository: Repository) -> RepositoryCell{
        let cell = Bundle.main.loadNibNamed("RepositoryCell", owner: self, options: nil)?.first as! RepositoryCell
        
        cell.forksLabel.attributedText = UILabel.returnAttributedTextWith(imageName: "fork.png",
                                                                          stringToAppend: "\(repository.forks!)",
                                                                          bounds: nil)
        
        cell.starsLabel.attributedText = UILabel.returnAttributedTextWith(imageName: "star.png",
                                                                          stringToAppend: "\(repository.stars!)",
                                                                          bounds: nil)
        
        cell.repositoryNameLabel.text = repository.name != nil ? "\(repository.name!)" : "Unnamed Repository"
        cell.usernameLabel.text = repository.description != nil ? "\(repository.owner!.login!)" : "Unnamed Owner"
        cell.descriptionLabel.text = repository.description != nil ? "\(repository.description!)" : ""
        cell.fullnameLabel.text = repository.fullName != nil ?  "\(repository.fullName!)" : ""
        cell.ownerPicture.setImage(withUrl: NSURL(string: repository.owner!.pictureUrl!) as! URL)
        
        return cell
    }
    
}

extension UILabel
{
    static func returnAttributedTextWith(imageName: String, stringToAppend: String, bounds: CGRect?) -> NSAttributedString
    {
        let attributedString = NSMutableAttributedString(string: "")
        
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: imageName)
        attachment.bounds = bounds ?? CGRect(x: 0, y: -5, width: 20, height: 20)
        
        attributedString.append(NSAttributedString(attachment: attachment))
        attributedString.append(NSAttributedString(string: stringToAppend))
        
        return attributedString
    }
}
