//
//  ViewController.swift
//  GitHubIntegration
//
//  Created by Thiago Mattara on 03/12/16.
//  Copyright © 2016 Thiago Mattara. All rights reserved.
//

import UIKit
import Alamofire
import When
import UIScrollView_InfiniteScroll
import LinearProgressBarMaterial
import MapleBacon

class RepositoriesViewController: UITableViewController {
    
    var repositories:[Repository] = [Repository]()
    var pageCount: Int = 1
    let linearBar: LinearProgressBar = LinearProgressBar()
    
    // MARK: Override UIViewController Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.linearBar.progressBarColor = UIColor(red: 0.85, green: 0.56, blue: 0.18, alpha: 1)
        self.loadRepositories(tableView:self.tableView!)
        
        self.tableView.infiniteScrollIndicatorStyle = UIActivityIndicatorViewStyle.gray
        self.tableView.addInfiniteScroll { (tableView) -> Void in self.loadRepositories(tableView: tableView) }
    }
    
    override func didReceiveMemoryWarning() {
        MapleBaconStorage.sharedStorage.clearMemoryStorage()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueRepositoryToPullRequests" {
            if self.tableView.indexPathForSelectedRow != nil {
                (segue.destination as! PullRequestsViewController).repository = self.repositories[self.tableView.indexPathForSelectedRow!.row]
            }
        }
    }
    
    // MARK: UITableViewController DataSource  Methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return self.repositories.count }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return RepositoryCell.returnCellFromRepository(repository: self.repositories[indexPath.row])
    }
    
    // MARK: UITableViewController Delegate  Methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "segueRepositoryToPullRequests", sender: self)
    }
    
    // MARK: Private Methods
    
    private func loadRepositories(tableView: UITableView) -> Void {
        self.linearBar.startAnimation()
        RepositoryService.SearchRepositories(query: "language:Java", sortBy: "stars", page: pageCount)
        .then({
            repositories in
            self.repositories.append(contentsOf:repositories)
            self.pageCount += 1

            self.tableView.reloadData()
            self.tableView.finishInfiniteScroll()
            self.linearBar.stopAnimation()
        })
    }
}

