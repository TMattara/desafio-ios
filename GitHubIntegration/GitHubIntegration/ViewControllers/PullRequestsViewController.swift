//
//  PullRequestsViewController.swift
//  GitHubIntegration
//
//  Created by Thiago Mattara on 03/12/16.
//  Copyright © 2016 Thiago Mattara. All rights reserved.
//

import Foundation
import UIKit
import LinearProgressBarMaterial
import MapleBacon

class PullRequestsViewController: UITableViewController {
    
    var repository: Repository!
    var pullRequests = [PullRequest]()
    let linearBar: LinearProgressBar = LinearProgressBar()
    
    // MARK: Override UIViewController Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.linearBar.progressBarColor = UIColor(red: 0.85, green: 0.56, blue: 0.18, alpha: 1)
        self.title = self.repository.name
        
        self.loadPullRequests()
    }
    
    override func didReceiveMemoryWarning() {
        MapleBaconStorage.sharedStorage.clearMemoryStorage()
    }
    
    // MARK: UITableViewController DataSource  Methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { return self.pullRequests.count }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return PullRequestCell.returnCellFromPullRequest(pullRequest: self.pullRequests[indexPath.row])
    }
    
    // MARK: UITableViewController Delegate  Methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UIApplication.shared.openURL(NSURL(string:self.pullRequests[indexPath.row].url!) as! URL)
    }
    
    // MARK: Private Methodss
    
    private func loadPullRequests(){
        self.linearBar.startAnimation()
        RepositoryService.GetPullRequestsFromRepository(repository: self.repository)
        .then({
            pullRequests in
            self.pullRequests.append(contentsOf: pullRequests)
            self.tableView.reloadData()
            self.linearBar.stopAnimation()
        })
    }
}
