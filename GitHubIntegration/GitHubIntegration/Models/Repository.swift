//
//  Repository.swift
//  GitHubIntegration
//
//  Created by Thiago Mattara on 03/12/16.
//  Copyright © 2016 Thiago Mattara. All rights reserved.
//

import Foundation
import ObjectMapper

class Repository: Mappable {
    
    var name: String?
    var description: String?
    var owner : User?
    var stars : Int?
    var forks : Int?
    var fullName: String?
    
    required init(){ }
    
    required init?(map: Map){ }

    func mapping(map: Map){
        self.name <- map["name"]
        self.description <- map["description"]
        self.owner <- map["owner"]
        self.stars <- map["stargazers_count"]
        self.forks <- map["forks"]
        self.fullName <- map["full_name"]
    }
    
}


