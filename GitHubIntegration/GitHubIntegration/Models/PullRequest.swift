//
//  PullRequest.swift
//  GitHubIntegration
//
//  Created by Thiago Mattara on 03/12/16.
//  Copyright © 2016 Thiago Mattara. All rights reserved.
//

import Foundation
import ObjectMapper

class PullRequest: Mappable {
    private var _date: String?

    var owner: User?
    var title: String?
    var body: String?
    var url: String?
    var date: Date? {
        get {
            
            if self._date == nil {
                return nil
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            
            return dateFormatter.date(from: self._date!)
        }
    }
    
    required init (){ }
    
    required init?(map: Map){ }
    
    func mapping(map: Map){
        self.title <- map["title"]
        self._date <- map["created_at"]
        self.body <- map["body"]
        self.owner <- map["user"]
        self.url <- map["html_url"]
    }
}

