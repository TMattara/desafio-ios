//
//  User.swift
//  GitHubIntegration
//
//  Created by Thiago Mattara on 03/12/16.
//  Copyright © 2016 Thiago Mattara. All rights reserved.
//

import Foundation
import ObjectMapper

class User: Mappable {
    
    var login: String?
    var pictureUrl: String?
    
    required init() { }
    
    required init?(map: Map){ }
    
    func mapping(map: Map){
        self.login <- map["login"]
        self.pictureUrl <- map["avatar_url"]
    }
}


